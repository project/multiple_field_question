(function ($) {
  Drupal.behaviors.multipleField = {
    attach: function (context, settings) {
      $('#quiz-question-answering-form').find('.messages').hide();
      $('.form-builder-bottom-title-bar').hide();
      $('.form-builder-title-bar').hide();
    }
  }
})(jQuery);