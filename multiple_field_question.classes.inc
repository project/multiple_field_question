<?php

/**
 * @file
 * Multiple Field Question extend for quiz question.
 */

/**
 *
 */
class MultipleFieldQuestion extends QuizQuestion {

  /**
   * Implements save.
   * Stores the question in the database.
   *
   * @param
   * is_new if - if the node is a new node.
   */
  public function saveNodeProperties($is_new = FALSE) {
    $is_new = $is_new || $this->node->revision == 1;
    // If new quiz insert values.
    if ($is_new) {
      db_insert('quiz_multiple_field_question_node_properties')
        ->fields(array(
          'nid' => $this->node->nid,
          'vid' => $this->node->vid,
          'type' => 1,
        ))
        ->execute();
    }
    // Update if not new quiz.
    else {
      db_update('quiz_multiple_field_question_node_properties')
        ->fields(array(
          'type' => 1,
        ))
        ->condition('nid', $this->node->nid)
        ->condition('vid', $this->node->vid)
        ->execute();
    }
  }

  /**
   * Implements validate.
   * QuizQuestion#validate().
   */
  public function validateNode(array &$form) {

  }

  /**
   * Implements delete.
   *
   * @see QuizQuestion#delete()
   */
  public function delete($only_this_version = FALSE) {
    // Delete values if Quiz Webform deletes.
    if ($only_this_version) {
      db_delete('quiz_multiple_field_question_node_properties')
        ->condition('question_nid', $this->node->nid)
        ->condition('question_vid', $this->node->vid)
        ->execute();
      db_delete('quiz_multiple_field_question_user_answers')
        ->condition('nid', $this->node->nid)
        ->condition('vid', $this->node->vid)
        ->execute();
    }
    else {
      db_delete('quiz_multiple_field_question_node_properties')
        ->condition('nid', $this->node->nid)
        ->execute();
      db_delete('quiz_multiple_field_question_user_answers')
        ->condition('question_nid', $this->node->nid)
        ->execute();
    }
    parent::delete($only_this_version);
  }

  /**
   * Implements getNodeView.
   */
  public function getNodeView() {
    $content = parent::getNodeView();
    return $content;
  }

  /**
   * Implements getNodeProperties.
   */
  public function getNodeProperties() {
    if (isset($this->nodeProperties)) {
      return $this->nodeProperties;
    }
    $props = parent::getNodeProperties();
    $this->nodeProperties = $props;
    return $props;
  }

  /**
   * Generates the question form.
   *
   * This is called whenever a question is rendered, either
   * to an administrator or to a quiz taker.
   */
  public function getAnsweringForm(array $form_state = NULL, $rid) {
    $form = parent::getAnsweringForm($form_state, $rid);
    module_load_include('inc', 'form_builder', 'includes/form_builder.admin');
    $data = form_builder_interface('multiple_field_question', $form_state['build_info']['args'][0]->nid);
    $values = '';
    foreach ($data[0] as $key => $value) {
      if ((strpos($key, 'sample_') !== FALSE)) {
        unset($data[0][$key]);
      }
      if (strpos($key, 'new_') !== FALSE) {
        if ($value['#type'] == 'checkboxes') {
          $form[$key] = array(
            '#type' => 'checkboxes',
            '#title' => t($value['#title']),
            '#options' => $value['#options'],
            '#required' => $value['#required'],
          );
        }
        elseif ($value['#type'] == 'radios') {
          $form[$key] = array(
            '#type' => 'radios',
            '#title' => t($value['#title']),
            '#options' => $value['#options'],
            '#required' => $value['#required'],
          );
        }
        elseif ($value['#type'] == 'select') {
          $form[$key] = array(
            '#type' => 'select',
            '#title' => t($value['#title']),
            '#options' => $value['#options'],
            '#required' => $value['#required'],
          );
        }
        elseif ($value['#type'] == 'textfield') {
          $form[$key] = array(
            '#type' => 'textfield',
            '#title' => t($value['#title']),
            '#maxlength' => 256,
            '#required' => $value['#required'],
          );
        }
        elseif ($value['#type'] == 'textarea') {
          $form[$key] = array(
            '#type' => 'textarea',
            '#title' => t($value['#title']),
            '#required' => $value['#required'],
          );
        }
        elseif ($value['#type'] == 'date') {
          $form[$key] = array(
            '#type' => 'date',
            '#title' => t($value['#title']),
            '#required' => $value['#required'],
          );
        }

        elseif ($value['#type'] == 'fieldset') {
          $fieldset = variable_get('fieldset_inline_' . arg(1), 0);
          $hide_labels = variable_get('hide_labels_' . arg(1), 0);
          if ($fieldset == 1) {
            $value['#attributes'] = array('class' => array('container-inline'));
          }
          $form[$key] = array(
            '#type' => 'fieldset',
            '#title' => $value['#title'],
            '#attributes' => $value['#attributes'],
            '#cols' => 60,
          );

          foreach ($value as $key1 => $value1) {
            if (strpos($key1, 'new_') !== FALSE) {
              $form[$key][$key1] = array(
                '#type' => $value[$key1]['#type'],
                '#title' => $value[$key1]['#title'],
                '#options' => isset($value[$key1]['#options']) ? $value[$key1]['#options'] : array(),
                '#required' => $value[$key1]['#required'],
              );
            }
          }
        }

        elseif ($value['#type'] == 'managed_file') {
          unset($value['#form_builder']);
          unset($value['#pre_render']);
          $form[$key] = array(
            '#size' => '30',
            '#type' => 'managed_file',
            '#title' => t($value['#title']),
            '#upload_location' => 'public://',
            '#required' => $value['#required'],
          );
        }
      }
    }
    drupal_add_js(drupal_get_path('module', 'multiple_field_question') . '/js/multiple_field_question.js');
    $form['question_type'] = array(
      '#type' => 'hidden',
      '#value' => 'quiz_multiple_field_question',
    );
    return $form;
  }

  /**
   * Implements getCreationForm.
   */
  public function getCreationForm(array &$form_state = NULL) {
    $form['question'] = array(
      '#type' => 'textarea',
      '#rows' => 2,
    );
    return $form;
  }

  /**
   * Implements getMaximumScore.
   *
   * @see QuizQuestion#getMaximumScore()
   */
  public function getMaximumScore() {
    return variable_get('quiz_multiple_field_question_default_score', 10);
  }

}

/**
 * Extension of QuizQuestionResponse.
 */
class MultipleFieldResponse extends QuizQuestionResponse {

  /**
   * ID of the answer.
   */
  protected $answerId = 0;
  protected $file = NULL;
  protected $questionNode;

  /**
   * Constructor.
   */
  public function __construct($result_id, stdClass $question_node, $tries = NULL) {
    parent::__construct($result_id, $question_node, $tries);
    $tries = $_FILES;
    $this->answer = $tries;
    $this->answerFeedback = "";
    if (!isset($result) || !is_object($result)) {
      $result = new stdClass();
    }
    $result->is_correct = TRUE;
    $this->evaluated = 0;
    $this->questionNode = $question_node;
    $response = $this->getResponse();
    $this->result_id = $result_id;
    // Question has been answered already. We fetch the answer data from the database.
    $r = db_query('SELECT * FROM {quiz_multiple_field_question_user_answers}
    WHERE question_nid = :question_nid AND question_vid = :question_vid AND result_id = :result_id', array(
      ':question_nid' => $question_node->nid,
      ':question_vid' => $question_node->vid,
      ':result_id' => $result_id,
    ))->fetchAssoc();
    if (is_array($r)) {
      $this->score = $r['score'];
      $this->answerId = $r['answer_id'];
      $this->evaluated = $r['is_evaluated'];
      $this->answerFeedback = $r['answer_feedback'];
    }
  }

  /**
   * Implements getResponse.
   *
   * @return answer
   */
  public function getResponse() {
    return $this->answer;
  }

  /**
   * Implements isValid.
   */
  public function isValid() {
    module_load_include('inc', 'form_builder', 'includes/form_builder.admin');
    $data = form_builder_interface('multiple_field_question', $this->question->nid);
    $values = array();
    $f1 = 0;
    foreach ($_POST as $key => $value) {
      if (strpos($key, 'sample_') !== FALSE) {
        $values[$key] = $value;
        if (empty($value)) {
          $f1 = 1;
        }
      }
      if (strpos($key, 'new_') !== FALSE) {
        $values[$key] = $value;
        if (empty($value)) {
          $f1 = 1;
        }
        if (is_array($value)) {
          foreach ($value as $key3 => $value3) {
            $values[$key3] = $value3;
            if (empty($value3)) {
              $f1 = 1;
            }
          }
        }
        if ($data[0][$key]['#form_builder']['element_type'] == 'number') {
          if (!is_numeric($value)) {
            return t('Only Numeric Values allowed in ' . $data[0][$key]['#title']);
          }
        }
        if ($data[0][$key]['#form_builder']['element_type'] == 'email') {
          if (!valid_email_address($value)) {
            return t('Email not valid in ' . $data[0][$key]['#title']);
          }
        }
        if ($data[0][$key]['#form_builder']['element_type'] == 'fieldset') {
          foreach ($data[0][$key] as $key1 => $value1) {
            if (strpos($key1, 'new_') !== FALSE) {
              if ($data[0][$key][$key1]['#form_builder']['element_type'] == 'number') {
                if (!is_numeric($_POST[$key][$key1])) {
                  return t('Only Numeric Values allowed in ' . $data[0][$key][$key1]['#title']);
                }
              }
              if ($data[0][$key][$key1]['#form_builder']['element_type'] == 'email') {
                if (!filter_var($_POST[$key][$key1], FILTER_VALIDATE_EMAIL)) {
                  return t('Email not valid in ' . $data[0][$key][$key1]['#title']);
                }
              }
            }
          }
        }
      }
      if ($key == 'files') {
        $file_values = $values[$key];
        foreach ($file_values as $key2 => $value2) {
          $values[$key2] = $value2;
          if (empty($value2)) {
            $f1 = 1;
          }
        }
      }
    }
    if ($f1 == 1) {
      return t('Each field is required.');
    }
    $results = db_query('SELECT *
    FROM {quiz_multiple_field_question_user_answers} qf
    WHERE question_nid = :nid AND question_vid = :vid AND result_id = :result_id', array(
        ':nid' => $this->questionNode->nid,
        ':vid' => $this->questionNode->vid,
        ':result_id' => $this->result_id,
      )
    )
      ->fetchAssoc();
    if (!isset($results['result_id'])) {
      $answer_id = db_insert('quiz_multiple_field_question_user_answers')
        ->fields(array(
          'result_id' => $this->result_id,
          'question_vid' => $this->questionNode->vid,
          'question_nid' => $this->questionNode->nid,
          'multiple_field_question_id' => 0,
          'score' => 0,
          'is_evaluated' => 0,
          'answer' => serialize($values),
          'multiple_field_question_data' => serialize($_POST),
        ))
        ->execute();
    }
    else {
      $answer_id = db_update('quiz_multiple_field_question_user_answers')
        ->fields(array(
          'multiple_field_question_data' => serialize($_POST),
          'answer' => serialize($values),
        ))
        ->condition('result_id', $this->result_id)
        ->condition('question_vid', $this->questionNode->vid)
        ->condition('question_nid', $this->questionNode->nid)
        ->execute();
    }
    return TRUE;
  }

  /**
   * Implementation of save.
   */
  public function save() {
    $this->answerId = db_insert('quiz_multiple_field_question_user_answers')
      ->fields(array(
        'result_id' => $this->result_id,
        'question_vid' => $this->question->vid,
        'question_nid' => $this->question->nid,
        'multiple_field_question_id' => 0,
        'multiple_field_question_data' => serialize($_POST),
        'score' => $this->getScore(FALSE),
        'is_evaluated' => $this->evaluated,
        'answer_feedback' => $this->answerFeedback,
      ))
      ->execute();
  }

  /**
   * Implementation of delete.
   */
  public function delete() {
    db_delete('quiz_multiple_field_question_user_answers')
      ->condition('question_nid', $this->question->nid)
      ->condition('question_vid', $this->question->vid)
      ->condition('result_id', $this->result_id)
      ->execute();
  }

  /**
   * Implements score.
   *
   * @return uint
   */
  public function score() {
    return (int) db_query('SELECT score FROM {quiz_multiple_field_question_user_answers}
      WHERE result_id = :result_id AND question_vid = :question_vid', array(
      ':result_id' => $this->result_id,
      ':question_vid' => $this->question->vid
    ))->fetchField();
  }

  /**
   * Implementation of getFeedbackValues().
   *
   * @see QuizQuestionResponse::getFeedbackValues()
   */
  public function getFeedbackValues() {

    $result_id = $this->question->answers[0]['result_id'];
    $form = array();
    $r = db_select('quiz_multiple_field_question_user_answers', 'ua');
    $r->leftJoin('node', 'n', 'n.nid=ua.question_nid');
    $rs = $r
      ->fields('ua')
      ->condition('ua.result_id', $result_id, '=')
      ->condition('ua.question_nid', $this->question->nid, '=')
      ->condition('ua.question_vid', $this->question->vid, '=')
      ->execute()
      ->fetchAssoc();
    $answers = unserialize($rs['multiple_field_question_data']);
    module_load_include('inc', 'form_builder', 'includes/form_builder.admin');
    $data = form_builder_interface('multiple_field_question', $this->question->nid);
    $values = array();
    foreach ($rs as $key0 => $result) {
      if ($key0 == 'multiple_field_question_data') {
        $answers = unserialize($rs['multiple_field_question_data']);
      }
      $i = 1;
      $check_value = '';
      foreach ($data[0] as $key => $value) {
        if ((strpos($key, 'new_') !== FALSE)) {
          if (($value['#type'] != 'checkboxes') && ($value['#type'] != 'date') && ($value['#type'] != 'fieldset') && ($value['#type'] != 'managed_file')) {
            $anwser_value = '';
            if (isset($value['#field_prefix'])) {
              $anwser_value .= $value['#field_prefix'] . ' ';
            }
            if (!empty($answers['question'][$rs['question_nid']]['answer'][$key])) {
              $anwser_value .= $answers['question'][$rs['question_nid']]['answer'][$key];
            }
            if (isset($value['#field_suffix'])) {
              $anwser_value .= ' ' . $value['#field_suffix'];
            }
            if (!empty($anwser_value)) {
              $rows['r' . $i] = array(
                'c1' => $value['#title'],
                'c2' => $anwser_value,
              );
            }
            else {
              $rows['r' . $i] = array(
                'c1' => $value['#title'],
                'c2' => '',
              );
            }
          }

          elseif ($value['#type'] == 'managed_file') {
            $file_name = $value['#key'];
            if (!empty($answers['question'][$rs['question_nid']]['answer'][$file_name]['fid'])) {
              $anwser_value = $answers['question'][$rs['question_nid']]['answer'][$file_name]['fid'];
              $file_load = file_load($answers['question'][$rs['question_nid']]['answer'][$file_name]['fid']);
              if (!empty($file_load)) {
                $file_path = file_create_url($file_load->uri);
                $rows['r' . $i] = array(
                  'c1' => $value['#title'],
                  'c2' => '<a href="' . $file_path . '" target="_blank">' . $file_load->filename . '</a>',
                );
              }
              else {
                $rows['r' . $i] = array(
                  'c1' => $value['#title'],
                  'c2' => '',
                );
              }
            }
            else {
              $rows['r' . $i] = array(
                'c1' => $value['#title'],
                'c2' => '',
              );
            }
          }
          elseif ($value['#type'] == 'date') {
            if (!empty($answers['question'][$rs['question_nid']]['answer'][$key])) {
              $date_array = $answers['question'][$rs['question_nid']]['answer'][$key];
              $anwser_value = $date_array['month'] . '/' . $date_array['day'] . '/' . $date_array['year'] . ' (M/D/Y)';
              if ($anwser_value != '// (M/D/Y)') {
                $rows['r' . $i] = array(
                  'c1' => $value['#title'],
                  'c2' => $anwser_value,
                );
              }
            }
          }
          else {
            if (isset($answers['question'][$rs['question_nid']]['answer'][$key])) {
              foreach ($answers['question'][$rs['question_nid']]['answer'][$key] as $key1 => $value1) {
                $check_value .= $value1 . ', ';
              }
              if ($check_value != ', ') {
                $rows['r' . $i] = array(
                  'c1' => $value['#title'],
                  'c2' => rtrim($check_value, ', '),
                );
              }
            }
          }
          $i++;
        }
      }
    }
    if (!empty($rows)) {
      $form['submitted_data'] = array(
        '#theme' => 'table',
        '#header' => array(t('Field Name'), t('Answer')),
        '#rows' => $rows,
      );
    }
    if ($this->question && !empty($this->question->answers)) {
      $answer = (object) current($this->question->answers);
    }
    $datas[] = array(
      // Hide this column. Does not make sense for long answer as there are no choices.
      'choice' => NULL,
      'attempt' => drupal_render($form),
      'correct' => $this->question->answers[0]['is_correct'] ? quiz_icon('correct') : '',
      'score' => !$this->evaluated ? t('This answer has not yet been scored.') : $this->getScore(),
      'answer_feedback' => check_markup($this->answerFeedback),
    );
    return $datas;
  }

  /**
   * Getting Feedback of quiz.
   */
  public function getReportFormAnswerFeedback() {
    return array(
      '#title' => t('Enter feedback'),
      '#type' => 'textarea',
      '#default_value' => $this->answerFeedback,
      '#attributes' => array('class' => array('quiz-report-score')),
    );
  }

  /**
   * Implements getReportFormSubmit.
   */
  public function getReportFormSubmit() {
    return 'multiple_field_question_report_submit';
  }

}

/**
 * Extending form builder classes.
 */
class MultipleFieldQuestionForm extends FormBuilderFormBase {
  const LABEL_PREFIX = 'progressbar_label_first';

  /**
   * Add components to the form.
   *
   * @param array $components
   *   A components array as you would find it in $node->webform['components'].
   */
  public function addComponents($components) {
    foreach ($components as $cid => $component) {
      $element['#webform_component'] = $component;
      $element['#weight'] = $component['weight'];
      $element['#key'] = $component['form_key'];
      $parent_id = $component['pid'] ? 'cid_' . $component['pid'] : FORM_BUILDER_ROOT;
      $element['#form_builder'] = array(
        'element_id' => 'cid_' . $cid,
        'parent_id' => $parent_id,
      );
      if ($map = _form_builder_webform_property_map($component['type'])) {
        // Keep the internal type of this element as the component type. For example
        // this may match an $element['#type'] of 'webform_date' and set the
        // $element['#form_builder']['element_type'] to simply 'date'.
        if (isset($map['form_builder_type'])) {
          $element['#form_builder']['element_type'] = $map['form_builder_type'];
        }
      }
      if ($e = form_builder_webform_component_invoke($component['type'], 'form_builder_load', $element)) {
        $element = $e;
      }
      $this->setElementArray($element, $parent_id);
    }
  }

  /**
   * Create a webform component array based the form_builder cache.
   *
   * @param string $element_id
   *   Unique ID of the element.
   *
   * @return array
   *   A webform component array.
   */
  public function getComponent($element_id) {
    module_load_include('inc', 'form_builder_webform', 'form_builder_webform.components');

    $element = $this->getElementArray($element_id);
    $component = $element['#webform_component'];
    $type = $component['type'];

    $component['email'] = 0;
    $component['nid'] = $this->formId;
    $component['weight'] = $element['#weight'];
    // The component can't decide this on it's own.
    $component['pid'] = 0;
    $component['form_builder_element_id'] = $element_id;

    return $component;
  }

  /**
   *
   */
  public function updateNode($node) {
    $components = $this->getComponents($node);
    // $node->webform['components'] = $components;.
  }

}

/**
 *
 */
class MultipleFieldQuestionElement extends FormBuilderElementBase {

  /**
   * {@inheritdoc}
   */
  public function render() {
    $element = $this->element;
    if (isset($element['#webform_component'])) {
      $component = $element['#webform_component'];
      $new_element = webform_component_invoke($component['type'], 'render', $component, NULL, FALSE);
      // Preserve the #weight. It may have been changed by the positions form.
      $new_element['#weight'] = $element['#weight'];
      $new_element['#key'] = $element['#key'];
      $new_element['#webform_component'] = $component;
      $new_element['#form_builder'] = $element['#form_builder'];
      return $this->addPreRender($new_element);
    }
    return $this->addPreRender($element);
  }

  /**
   *
   */
  public function title() {
    return $this->element['#webform_component']['name'];
  }

  /**
   * {@inheritdoc}
   */
  protected function setProperty($property, $value) {
    $component = &$this->element['#webform_component'];
    $properties = $this->getProperties();
    $properties[$property]->setValue($component, $value);
  }

}

/**
 *
 */
class MultipleFieldQuestionProperty extends FormBuilderPropertyBase {

  protected $storageParents;

  /**
   * {@inheritdoc}
   */
  public function __construct($property, $params, $form_type_name) {
    $params += array('storage_parents' => array($property));
    parent::__construct($property, $params, $form_type_name);
    $this->storageParents = $params['storage_parents'];
  }

  /**
   * {@inheritdoc}
   */
  public function setValue(&$component, $value) {
    drupal_array_set_nested_value($component, $this->storageParents, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function form(&$form_state, $element) {
    // We use the rendered element here to re-use the form-API functions.
    $e = $element->render();
    $e += array("#{$this->property}" => $this->getValue($e['#webform_component']));
    // Set weight to just anything. Element positions aren't configured in
    // this way in form_builder.
    $e['#webform_component']['weight'] = 0;
    if (isset($this->params['form']) && function_exists($this->params['form'])) {
      $function = $this->params['form'];
      return $function($form_state, $this->formTypeName, $e, $this->property);
    }
    return array();
  }

  /**
   * Read the value from a component array.
   */
  public function getValue($component) {
    return drupal_array_get_nested_value($component, $this->storageParents);
  }

}


/**
 * -------------------------.
 */
class QuizBldrForm extends FormBuilderFormBase {
  const LABEL_PREFIX = 'progressbar_label_first';

  /**
   *
   */
  public function updateNode($node) {
    $components = $this->getComponents($node);
    $first = reset($components);
    if ($first['type'] == 'pagebreak') {
      // $node->webform['progressbar_label_first'] = $first['name'];
      // Remove pagebreak if it has the right key.
      if (substr($first['form_key'], 0, strlen(self::LABEL_PREFIX)) === self::LABEL_PREFIX) {
        unset($components[$first['cid']]);
        foreach ($components as &$c) {
          $c['page_num']--;
        }
      }
    }
    // $node->webform['components'] = $components;.
  }

}

/**
 *
 */
class QuizBldrElement extends FormBuilderElementBase {


}

/**
 *
 */
class QuizBldrProperty extends FormBuilderPropertyBase {


}
