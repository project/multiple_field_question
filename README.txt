
/**
 * @file
 * README file for Quiz Multiple Field Question.
 */

Quiz Multiple Field Question
Allows quiz creators to make a multiple fields question type.


CONTENTS OF FILE
----------------
 * Introduction
 * Requirements
 * Installation
 * Usage

INTRODUCTION
------------
 * Multiple field question quiz module allows quiz questions to be built with  
   multiple fields and also supports different field types.
 * This module depends on the form builder module.
 * Module allows Checkboxes, TextField, textarea, radios, selectlist, 
   date, number, Fieldset, Image and Email.
 * Scoring will always be manual.
 * Questions can be made to fall on a single line.


REQUIREMENTS
------------
This module requires the following modules:
 * form_builder (Contrib module)
 * quiz (Contrib module)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * Else Goto admin/modules and enable "Multiple Field Question".

USAGE
-----
* After enabling this module, a content type 
  "Multiple Field Question" will be created with default quiz save option.
* Then create a node and assign to any Quiz and then take test.
